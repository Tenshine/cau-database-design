/*
 ** results.js
 */

class Result {
    constructor(message) {
        this.message = message;
    }
}

module.exports.Error = class extends Result {
    constructor(message) {
        super(message);
    }
}

module.exports.Success = class extends Result {
    constructor(message) {
        super(message);
    }
}

module.exports.Failure = class extends Result {
    constructor(message) {
        super(message);
    }
}
