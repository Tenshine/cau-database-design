/*
 ** auth.js
 */

const results = require('../utils/results');

let database = null
let passport = null

function authenticate(req, res, strategy, callback) {
    passport.authenticate(strategy, function (err, user) {
        if (err) {
            switch (err.code) {
                case 'ER_DUP_ENTRY':
                    callback(new results.Failure('Username already taken'));
                    return;
                default:
                    callback(new results.Error(err));
                    return;
            }
        }
        if (!user) {
            callback(new results.Failure('Authentification Error, invalid username or password'));
            return;
        }
        req.logIn(user, function (err) {
            callback(err ? new results.Error(err) : new results.Success('Success'))
        });
    })(req, res)
}

/* #region  Endpoints */

function setHTTPEndpoints(router, handle) {
    /* post /auth/signin-local: local authentification with passport */
    router.post('/local-signin', function (req, res) {
        authenticate(req, res, 'local', function (result) {
            handle(req, res, result)
        })
    })
    /* post /auth/signup-local: local authentification with passport */
    router.post('/local-signup', function (req, res) {
        authenticate(req, res, 'local-signup', function (result) {
            handle(req, res, result)
        })
    })
}

/* #endregion */

module.exports = function (Router, Handle, Database, Passport) {
    database = Database;
    passport = Passport;

    setHTTPEndpoints(Router, Handle);
    return Router
};
