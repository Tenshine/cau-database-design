/*
 ** events.js
 */

const results = require('../utils/results');

let database = null
let passport = null

function getEvents(params, callback) {
    params.limit = params.limit && parseInt(params.limit) || 10

    database.getConnection(function (err, con) {
        if (err)
            return callback(new results.Error("Cannot connect to database"))
        try {
            con.query(`SELECT * FROM event where public=true LIMIT ${params.limit};`, function (err, res) {
                if (err)
                    throw err
                if (!res.length)
                    return callback(new results.Failure('Event not found'))
                var events = res
                var ids = res.map(event => event.id).join(",")
                if (ids) {
                    con.query(`SELECT event_id, firstname, lastname from response where event_id in (${ids})`, (err, res) => {
                        if (err)
                            throw err
                        for (event of events) {
                            event.responses = res.filter(response => {
                                return response.event_id == event.id
                            })
                            console.log(res.filter(response => response.event_id == event.id))
                            event.responses.forEach(obj => delete obj.response_id)
                        }
                        return callback(new results.Success(events))
                    })
                } else {
                    for (event of events) {
                        event.images = []
                    }
                    callback(new results.Success(events));
                }
            })

        } catch (ex) {
            con.release();
            return callback(new results.Error(ex))
        }
    })
}

function getEvent(id, callback) {
    database.getConnection(function (err, con) {
        if (err)
            return callback(new results.Error("Cannot connect to database"))
        try {
            con.query(`SELECT * FROM event where id=${id};`, function (err, res) {
                if (err)
                    throw err
                if (!res.length)
                    return callback(new results.Failure('Project not found'))
                var event = res[0]
                con.query(`SELECT firstname, lastname, status from response where event_id=${res[0].id}`, (err, res) => {
                    event.responses = res
                    return callback(new results.Success(event))
                })
            })

        } catch (ex) {
            con.release();
            return callback(new results.Error(ex))
        }
    })
}

function createEvent(params, callback) {
    database.query(`INSERT INTO event (owner_id, title, description, public, date) values ('${params.user_id}', '${params.title}', '${params.description}', ${params.public}, '${params.date}')`, (err, res) => {
        if (err)
            return callback(new results.Error(err))
        callback(new results.Success("Success"));
    });
}

function respondToEvent(params, callback) {
    database.query(`INSERT INTO response (event_id, firstname, lastname, status)
        SELECT '${params.event_id}', '${params.firstname}', '${params.lastname}', ${params.response} FROM DUAL 
        WHERE NOT EXISTS (SELECT * FROM response
        WHERE \`event_id\`='${params.event_id}' AND \`firstname\`='${params.firstname}' AND \`lastname\`='${params.lastname}' LIMIT 1);`, (err, res) => {
            if (err)
                return callback(new results.Error(err))
            callback(new results.Success("Success"));
        });
}

/* #region  Endpoints */

function setHTTPEndpoints(router, handle) {
    router.get('', function (req, res) {
        getEvents(req.query, function (result) {
            handle(req, res, result);
        });
    });

    router.get('/:id', function (req, res) {
        getEvent(req.params.id, function (result) {
            handle(req, res, result);
        });
    });

    router.post('', passport.isAuthenticated(handle), function (req, res) {
        params = {
            user_id: req.user.id || -1,
            title: req.body.title || '',
            description: req.body.description || '',
            public: req.body.public || true,
            date: req.body.date || new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '').split(' ')[0]
        };
        createEvent(params, function (result) {
            handle(req, res, result);
        });
    });

    router.post('/respond', function (req, res) {
        params = {
            event_id: req.body.event_id || -1,
            firstname: req.body.firstname || req.user.firstname,
            lastname: req.body.lastname || req.user.lastname,
            response: req.body.response !== undefined ? req.body.response : true
        };
        respondToEvent(params, function (result) {
            handle(req, res, result);
        });
    });
}

/* #endregion */

module.exports = function (Router, Handle, Database, Passport) {
    database = Database;
    passport = Passport;

    setHTTPEndpoints(Router, Handle);
    return Router;
};
