/*
** api.js
*/

const express = require('express')
const results = require('./utils/results')

const auth = require("./api/auth")
const events = require("./api/event")

function handle(req, res, result) {
    switch (result.constructor) {
        case results.Error:
            console.log(req.method, req.originalUrl + ':', result.message);
            res.status(500);
            res.json('Something went wrong');
            break;
        case results.Success:
            res.json(result.message);
            break;
        case results.Failure:
            res.status(400);
            res.json(result.message);
            break;
        default:
            console.log(req.method, req.url + ':', result);
            res.status(500);
            res.json('The impossible happend !');
            break;
    }
}

module.exports = function (app, passport, database) {
    app.use('/auth', auth(express.Router(), handle, database, passport));
    app.use('/events', events(express.Router(), handle, database, passport));
}
