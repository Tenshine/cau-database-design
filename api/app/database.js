/*
 ** database.js
 */

const fs = require('fs');
const mysql = require('mysql');

var LogStream = fs.createWriteStream(".sql.logs");

//#region Members

var pool;
var ConConfig;

//#endregion Members


//#region Methods

function connectionCallback(err, callback) {
    if (err) {
        onConnectionFailure(err, callback);
    } else {
        onConnectionSuccess(callback);
    }
}

function onConnectionFailure(err, callback) {
    console.log('Error:', err.code);

    pool = mysql.createPool(ConConfig);
};

function onConnectionSuccess(callback) {
    console.log('MySQL Connected');
    query('use ' + ConConfig.database, (err, res) => {
        callback(err);
    })
}

//#endregion Methods

module.exports.createPool = function (connectConfig, callback) {
    ConConfig = connectConfig;
    pool = mysql.createPool(ConConfig);
}

module.exports.getConnection = function (callback) {
    pool.getConnection(callback)
}

module.exports.query = function (query, callback) {
    LogStream.write(query + '\n\n', function (err) {
        if (err) {
            console.log(err);
        }
    });
    pool.query(query, callback);
}

module.exports.rollback = function (con, err, callback) {
    con.rollback(err => {})
    con.release();
    callback(err)
}

module.exports.escape = mysql.escape
module.exports.escapeId = mysql.escapeId