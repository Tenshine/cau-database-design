module.exports = function (passport, database) {

    var bcrypt = require('bcrypt');
    var results = require('./utils/results')

    const LocalStrategy = require('passport-local').Strategy;

    passport.isAuthenticated = function (handle) {
        return function (req, res, next) {
            if (req.isAuthenticated()) {
                next();
            } else {
                handle(req, res, new results.Failure("User not logged"))
            }
        }
    }

    passport.serializeUser(
        function (user, done) {
            done(null, user.id);
        });

    passport.deserializeUser(
        function (id, done) {
            const query = `SELECT * FROM user WHERE id=${id}`
            database.query(query, function (err, res) {
                done(err, res[0] || null);
            })
        });

    passport.use(new LocalStrategy(function (username, password, done) {
            database.query(`SELECT * FROM user WHERE username="${username}"`, function (err, res) {
                if (err)
                    return done(err);
                return done(null, (res[0] && bcrypt.compareSync(password, res[0].password)) ? res[0] : false)
            });
        }));

    passport.use('local-signup', new LocalStrategy({ passReqToCallback: true },
        function (req, username, password, done) {
            const saltRounds = 10;
            var salt = bcrypt.genSaltSync(saltRounds);
            var hash = bcrypt.hashSync(password, salt);

            const query = `insert into user (username, password, firstname, lastname) values (
                '${username}',
                '${hash}',
                '${req.body.firstname}',
                '${req.body.lastname}');`

            database.query(query, function (err, res) {
                if (err)
                    return done(err)
                database.query(`SELECT * FROM user WHERE username="${username}"`, function (err, res) {
                    if (err)
                        return done(err)
                    done(null, res[0] || false)
                })
            });
        }));
}
