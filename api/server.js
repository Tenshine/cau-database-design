var express = require('express');
var session = require('cookie-session')
var cookieParser = require('cookie-parser')
var bodyParser = require('body-parser')
var app = express()

const passport = require('passport')
const cors = require('cors')

const database = require('./app/database')
const api = require("./app/api")

function main() {
    app.use(cors({
        origin: ['http://localhost:3000', 'http://localhost:8888'],
        credentials: true,
        methods: 'GET, POST',
        allowedHeaders: ['Content-Type', 'Set-Cookie', '*']
}))
    app.use(session({
        name: 'session',
        secret: 'testoboto',
        resave: false,
        saveUninitialized: false
    }));
    app.use(cookieParser())
    app.use(bodyParser.urlencoded({ "limit" : '10mb', 'extended': 'true' }))
    app.use(bodyParser.json({ "limit" : '10mb', 'extended': 'true'}))
    app.use(passport.initialize())
    app.use(passport.session())

    require('./app/passport')(passport, database)
    api(app, passport, database)

    console.log(`App listening on port 5072`)
    app.listen(5072)
}

database.createPool({
    host: process.env.NODE_ENV == "docker" ? "database" : "localhost",
    user: "api",
    port: process.env.NODE_ENV == "docker" ? 3306 : 4455,
    password: "database",
    database: "design",
    connectionLimit: 20,
    insecureAuth: true,
    multipleStatements: true,
    timezone: 'UTC'
});

main()