USE design;

CREATE TABLE IF NOT EXISTS user (
    id INT AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(255),
    password VARCHAR(255),
    firstname VARCHAR(255),
    lastname VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS event (
    id INT AUTO_INCREMENT PRIMARY KEY,
    owner_id INT not null,
    title VARCHAR(255),
    description VARCHAR(2048),
    public BOOLEAN,
    date DATE,
    FOREIGN KEY (owner_id)
        REFERENCES user (id)
        ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS response (
    event_id INT not null,
    firstname VARCHAR(255),
    lastname VARCHAR(255),
    status BOOLEAN,
    FOREIGN KEY (event_id)
        REFERENCES event (id)
        ON DELETE CASCADE
);