import React from 'react';
import { Switch, Route, Link } from 'react-router-dom'

import LandingPage from './containers/LandingPage/LandingPage'
import RegisterPage from './containers/Register/Register'
import LoginPage from './containers/Login/Login'
import DashboardPage from './containers/Dashboard/Dashboard'
import CreateEventPage from './containers/Event/CreateEvent'
import EventPage from './containers/Event/Event'
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route path="/" exact strict component={LandingPage} />
        <Route path="/signup" exact strict component={RegisterPage} />
        <Route path="/login" exact strict component={LoginPage} />
        <Route path="/dashboard" component={DashboardPage} />
        <Route path="/createEvent" component={CreateEventPage} />
        <Route path="/event/:id" component={EventPage} />
      </Switch>
    </div>
  );
}

export default App;
