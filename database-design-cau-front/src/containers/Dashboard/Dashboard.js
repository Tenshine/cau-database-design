import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

import './Dashboard.css'

const DashboardPage = () => {
  const [events, setEvents] = React.useState([])

  React.useEffect(() => {
    axios({
      method: 'GET',
      url: '/events',
    }).then((response) => {
      setEvents(response.data)
    }).catch((err) => console.log(err))
  }, [])

  return (
    <div className="Dashboard">
      <div className="Dashboard-title">
        <span>
          Dashboard
        </span>
        <Link to={'/createEvent'} className="Dashboard-title-button">
          Create an event
        </Link>
      </div>

      <div className="Dashboard-grid">
        {events.map((event, idx) => (
          <div className="Dashboard-grip-item" key={idx}>
            <div style={{ fontWeight: 'bold', fontSize: '1.5em', marginBottom: '0.2em' }}>
              {event.title}
            </div>
            <div>
              {event.description}
            </div>
            <div style={{ marginBottom: '1em' }}>
              Date: {event.date.substring(0, 10)}
            </div>
            <Link to={`/event/${event.id}`} style={{ color: 'green' }}>
              See the event
            </Link>
          </div>
        ))}
      </div>
    </div>
  )
}

export default DashboardPage