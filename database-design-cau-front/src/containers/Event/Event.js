import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

const EventPage = ({ match }) => {
  const [firstname, setFirstname] = React.useState('')
  const [lastname, setLastname] = React.useState('')
  const [response, setResponse] = React.useState(true)
  const [date, setDate] = React.useState('')
  const [event, setEvent] = React.useState({})

  React.useEffect(() => {
    if (match && match.params && match.params.id) {
      axios({
        method: 'GET',
        url: `/events/${match.params.id}`,
      }).then((response) => setEvent(response.data))
        .catch((err) => console.log(err))
    }
  }, [match])

  const submitForm = () => {
    if (firstname !== '' && lastname !== '' && event.id) {
      axios({
        method: 'POST',
        url: '/events/respond',
        data: {
          event_id: event.id,
          firstname,
          lastname,
          response: response,
        },
      }).then((response) => {
        console.log(response.data)
      }).catch((err) => console.log(err))
    }
  }

  console.log(response)

  return (
    <div className="App-header">
      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', gridColumnGap: '1em' }}>
        <div>
          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <span>Event:</span>
            <span style={{ fontWeight: 'bold', marginBottom: '1em', fontSize: '1.5em' }}>{event && event.title}</span>
            <span>Description:</span>
            <span style={{ marginBottom: '2em' }}>{event && event.description}</span>
            <span style={{ marginBottom: '0.5em' }}>Join this event !</span>
          </div>

          <div style={{ display: 'flex', flexDirection: 'column' }}>
            <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
              <label htmlFor="firstname">Firstname</label>
              <input type="text" style={{ minHeight: '3em' }} value={firstname} onChange={(e) => setFirstname(e.target.value)} id="firstname" />
            </div>

            <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
              <label htmlFor="lastname">Lastname</label>
              <input type="text" style={{ minHeight: '3em' }} value={lastname} onChange={(e) => setLastname(e.target.value)} id="lastname" />
            </div>

            <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
              <label htmlFor="response">Participation</label>
              <button onClick={() => setResponse(!response)} id="response" style={{ minHeight: '4em', minWidth: '2em' }}>{response ? 'Participate' : 'Do not participate'}</button>
            </div>
          </div>

          <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
            <Link to={"/dashboard"} style={{ color: 'white', marginRight: '1em' }}>{'< Return'}</Link>
            <button style={{ width: '100%', height: '100%' }} onClick={() => submitForm()}>Add</button>
          </div>
        </div>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <div style={{ marginBottom: '2em' }}>These people answered :</div>

          <div style={{ height: '100%', overflowY: 'scroll' }}>
            {event && event.responses && event.responses.map((response, idx) => (
              <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', borderWidth: '0.01em', borderColor: 'white', borderStyle: 'solid' }}>
                <div>
                  {response.firstname} {response.lastname}
                </div>
                <div style={ response.status ? { color: 'green' } : { color: 'red'} }>
                  {response.status ? 'Participate' : 'Do not participate'}
                  </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default EventPage