import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

const CreateEventPage = () => {
  const [title, setTitle] = React.useState('')
  const [description, setDescription] = React.useState('')
  const [privacy, setPrivacy] = React.useState('')
  const [date, setDate] = React.useState('')

  const submitForm = () => {
    if (title !== '' && description !== '' && date !== '') {
      axios({
        method: 'POST',
        url: '/events',
        data: {
          title,
          description,
          public: privacy,
          date,
        },
      }).then((response) => {
        console.log(response.data)
      }).catch((err) => console.log(err))
    }
  }

  return (
    <div className="App-header">
      <span style={{ fontWeight: 'bold', fontSize: '2em', marginBottom: '1em' }}>Create an event</span>

      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
          <label htmlFor="title">Title</label>
          <input type="text" style={{ minHeight: '3em' }} value={title} onChange={(e) => setTitle(e.target.value)} id="title" />
        </div>

        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
          <label htmlFor="description">Description</label>
          <input type="text" style={{ minHeight: '3em' }} value={description} onChange={(e) => setDescription(e.target.value)} id="description" />
        </div>

        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
          <label htmlFor="privacy">Public</label>
          <input type="checkbox" style={{ minHeight: '2em', minWidth: '2em' }} value={privacy} onChange={(e) => setPrivacy(e.target.value)} id="privacy" />
        </div>

        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
          <label htmlFor="date">Date</label>
          <input type="text" style={{ minHeight: '3em' }} value={date} onChange={(e) => setDate(e.target.value)} id="date" placeholder="Year-Month-Day"/>
        </div>

        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
          <Link style={{ color: 'white', marginRight: '1em' }} to={'/dashboard'}>{'< Return'}</Link>
         <button style={{ width: '100%', height: '100%' }} onClick={() => submitForm()}>Create</button>
        </div>
      </div>
    </div>
  )
}

export default CreateEventPage