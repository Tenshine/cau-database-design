import React from 'react'
import axios from 'axios'
import { Link } from 'react-router-dom'

const RegisterPage = ({ history }) => {
  const [username, setUsername] = React.useState('')
  const [password, setPassword] = React.useState('')
  const [firstname, setFirstname] = React.useState('')
  const [lastname, setLastname] = React.useState('')

  const submitForm = () => {
    if (username !== '' && password !== '' && firstname !== '' && lastname !== '') {
      axios({
        method: 'POST',
        url: '/auth/local-signup',
        data: {
          username,
          password,
          firstname,
          lastname,
        },
      }).then((response) => {
        history.push('/dashboard')
      }).catch((err) => console.log(err))
    }
  }

  return (
    <div className="App-header">
      <span style={{ fontWeight: 'bold', fontSize: '2em', marginBottom: '1em' }}>Register Form</span>

      <div style={{ display: 'flex', flexDirection: 'column' }}>
      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
        <label htmlFor="username">Username</label>
        <input type="text" style={{ minHeight: '3em' }} value={username} onChange={(e) => setUsername(e.target.value)} id="username" />
      </div>

      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
        <label htmlFor="password">Password</label>
        <input type="password" style={{ minHeight: '3em' }} value={password} onChange={(e) => setPassword(e.target.value)} id="password" />
      </div>

      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
        <label htmlFor="firstname">Firstname</label>
        <input type="text" style={{ minHeight: '3em' }} value={firstname} onChange={(e) => setFirstname(e.target.value)} id="firstname" />
      </div>

      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
        <label htmlFor="lastname">Lastname</label>
        <input type="text" style={{ minHeight: '3em' }} value={lastname} onChange={(e) => setLastname(e.target.value)} id="lastname" />
      </div>

        <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em' }}>
      <Link style={{ color: 'white', marginRight: '1em' }} to={"/"}>{'< Return'}</Link>
      <button style={{ width: '100%', height: '100%' }} onClick={() => submitForm()} >Sign up</button>
        </div>
      </div>
    </div>
  )
}

export default RegisterPage