import React from "react";
import logo from "../../logo.svg";
import {Link} from "react-router-dom";

const LandingPage = () => {
  return (
    <div className="App-header">
      {/*<img src={logo} className="App-logo" alt="logo" />*/}
        <p>
          Database Design Team Project Fall 2019
        </p>
      <div>50191626 Xavier Hainneville</div>
      <div>50191658 Landry Noy</div>
      <div>50191653 Lucas Ricci</div>
      <div>50191630 Toerau Marot</div>
      <div>50191625 Yunhan Xia</div>
      <div style={{ display: 'grid', gridTemplateColumns: '1fr 1fr', justifyItems: 'center', alignItems: 'center', marginBottom: '0.5em', marginTop: '2em' }}>
      <Link to={'/signup'} style={{ color: 'white', marginRight: '1em', fontWeight: 'bold', fontSize: '1.5em' }}>
        Sign up
      </Link>
      <Link to={"/login"} style={{ color: 'white', fontWeight: 'bold', fontSize: '1.5em' }}>
        Login
      </Link>
      </div>
    </div>
  )
}

export default LandingPage